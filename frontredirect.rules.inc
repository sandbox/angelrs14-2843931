<?php
/**
 * @file
 * Rules implementation.
 */

/**
 * Implements hook_rules_action_info().
 */
function frontredirect_rules_action_info() {

  $items['frontredirect_action_user_path_update'] = array(
    'label' => t('Front redirect: user path update'),
    'group' => t('Front redirect'),
    'parameter' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'description' => t("Introduce the path value where the user will be redirected."),
        'optional' => FALSE,
      ),
      'uid' => array(
        'type' => 'text',
        'label' => t('User ID'),
        'description' => t("Introduce ID for specific users. If the field is blank, it will take the value of the user in session"),
        'optional' => TRUE,
      ),
    ),
  );


  return $items;
}

/**
 *
 * @global object $user
 * @param string $path
 *      redirect path
 * @param integer $uid
 *       User ID
 */
function frontredirect_action_user_path_update($path, $uid = NULL) {
  global $user;
  $uid = is_null($uid) ? $user->uid : $uid;
  if ($uid != 0) {
    frontredirect_user_path_update($uid, $path);
  }
}
