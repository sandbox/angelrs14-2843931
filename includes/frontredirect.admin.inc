<?php

/*
 * @param int $uid
 *   User id
 * @param string $path
 *   path
 *
 * @return string
 */

function frontredirect_settings_form($form, &$form_state) {

  $roles = user_roles();

  $form['frontredirect_roles'] = array(
    '#type' => 'container',
    '#title' => 'Path redirect',
    '#tree' => TRUE,
  );

  $default = variable_get('frontredirect_roles');

  foreach ($roles as $key => $role) {
    $form['frontredirect_roles'][$key] = array(
      '#type' => 'textfield',
      '#title' => $role,
      '#default_value' => $default[$key],
      '#size' => 60,
      '#required' => FALSE,
      '#description' => t('Default path redirect to @role role', array('@role' => $role)),
    );
  } 
  
  return system_settings_form($form);
}

// frontredirect_settings_form_validate

// frontredirect_settings_form_submit

